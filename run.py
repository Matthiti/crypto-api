import os
from dotenv import load_dotenv

load_dotenv()

from app import app

if bool(os.environ.get('DEBUG')):
    app.run(debug=True)
else:
    app.run(debug=False, host='0.0.0.0')
