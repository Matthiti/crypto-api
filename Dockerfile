# build stage
FROM python:3-slim AS builder

#RUN apk update && apk add --no-cache g++ libffi-dev musl-dev python3-dev libffi-dev openssl-dev mariadb-dev rust cargo
RUN apt update && apt install --no-install-recommends -y gcc build-essential libffi-dev libssl-dev python3-dev libmariadb-dev && rm -rf /var/lib/apt/lists/*

# Create virtualenv
RUN python -m venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

RUN pip install wheel

COPY requirements.txt ./
RUN pip install -r requirements.txt

# production stage
FROM python:3-slim

RUN apt update && apt install --no-install-recommends -y libmariadb-dev

COPY --from=builder /opt/venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

WORKDIR /code

COPY wsgi.py ./
COPY app/ ./app

# Create a non-root user
RUN groupadd -r python && useradd -g python pythonuser
USER pythonuser

EXPOSE 5000

CMD ["gunicorn", "-w", "1", "-b", "0.0.0.0:5000", "wsgi:app"]