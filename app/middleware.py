from flask import request, g
import jwt

from app.config import Config


def jwt_middleware():
    if 'X-Authorization' not in request.headers:
        return not_authorized("Missing 'X-Authorization' header")

    header = request.headers['X-Authorization']
    if not header.startswith('Bearer '):
        return not_authorized("Invalid 'X-Authorization' header")

    token = header.removeprefix('Bearer ')
    try:
        payload = jwt.decode(token, Config.JWT_PUBLIC_KEY, algorithms=['RS256'])
        if payload['domains'] is None or Config.DOMAIN_KEY not in payload['domains']:
            return {
                'success': False,
                'message': "You don't have access to this domain"
            }, 403
        g.user_uuid = payload['uuid']
    except jwt.exceptions.InvalidTokenError:
        return not_authorized("Invalid token")


def not_authorized(message: str):
    return {
       'success': False,
       'message': message
    }, 401
