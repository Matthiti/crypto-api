from flask import g
from sqlalchemy import func, case
from sqlalchemy.sql.functions import coalesce

from app import db
from app.models import Trade, Coin, Portfolio, Transaction


def _portfolio_exists(portfolio_id):
    return db.session.query(Portfolio.id).filter_by(
        user_uuid=g.get('user_uuid'),
        id=portfolio_id
    ).scalar() is not None


def _coin_exists(coin_id):
    return db.session.query(Coin.id).filter_by(id=coin_id).scalar() is not None


def _has_sufficient_amount(portfolio_id, coin_id, amount):
    result = db.session.query(
        coalesce(db.session.query(
            func.sum(
                case([(Trade.to_coin_id == Coin.id, Trade.to_amount)], else_=0)
                -
                case([(Trade.from_coin_id == Coin.id, Trade.from_amount)], else_=0)
            )
        ).join(Portfolio
        ).filter(Portfolio.id == portfolio_id
        ).as_scalar(
        ), 0)
        +
        coalesce(db.session.query(
            func.sum(
                case([(Transaction.coin_id == Coin.id,
                    case([(Transaction.is_deposit, Transaction.amount)], else_=-Transaction.amount)
                )], else_=0)
            )
        ).join(Portfolio
        ).filter(Portfolio.id == portfolio_id
        ).as_scalar(
        ), 0)
    ).filter(Coin.id == coin_id
    ).first()

    return int(result[0]) >= int(amount) if result is not None else False
