from operator import or_

import ccxt
from flask import Blueprint
from sqlalchemy import func, case, desc
from sqlalchemy.sql.functions import coalesce

from app import db
from app.models import Portfolio, Coin, PortfolioCoin, Trade, Transaction
from app.serializers import PortfolioCoinSchema, TransactionSchema, TradeSchema
from app.util import _portfolio_exists, _coin_exists
from app.views import success, error

portfolio_coin_api = Blueprint('Portfolio Coin API', __name__, url_prefix='/api/v1/portfolios/<portfolio_id>/coins')


@portfolio_coin_api.route('')
def all_portfolio_coins(portfolio_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    result = db.session.query(
        Coin,
        coalesce(db.session.query(
            func.sum(
                case([(Trade.to_coin_id == Coin.id, Trade.to_amount)], else_=0)
                -
                case([(Trade.from_coin_id == Coin.id, Trade.from_amount)], else_=0)
            )
        ).join(Portfolio
        ).filter(Portfolio.id == portfolio_id
        ).as_scalar(
        ), 0)
        +
        coalesce(db.session.query(
            func.sum(
                case([(Transaction.coin_id == Coin.id,
                    case([(Transaction.is_deposit, Transaction.amount)], else_=-Transaction.amount)
                )], else_=0)
            )
        ).join(Portfolio
        ).filter(Portfolio.id == portfolio_id
        ).as_scalar(
        ), 0)
    ).order_by(Coin.symbol
    ).all()

    coins = [PortfolioCoin(coin, int(amount) if amount is not None else 0, _get_current_price(coin)) for (coin, amount) in result]
    return success(PortfolioCoinSchema(many=True).dump(coins))


@portfolio_coin_api.route('/<coin_id>')
def find_portfolio_coin(portfolio_id, coin_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    result = db.session.query(
        Coin,
        coalesce(db.session.query(
            func.sum(
                case([(Trade.to_coin_id == Coin.id, Trade.to_amount)], else_=0)
                -
                case([(Trade.from_coin_id == Coin.id, Trade.from_amount)], else_=0)
            )
        ).join(Portfolio
        ).filter(Portfolio.id == portfolio_id
        ).as_scalar(
        ), 0)
        +
        coalesce(db.session.query(
            func.sum(
                case([(Transaction.coin_id == Coin.id,
                    case([(Transaction.is_deposit, Transaction.amount)], else_=-Transaction.amount)
                )], else_=0)
            )
        ).join(Portfolio
        ).filter(Portfolio.id == portfolio_id
        ).as_scalar(
        ), 0)
    ).filter(Coin.id == coin_id
    ).first()

    if result is None:
        return error('Coin with ID {} not found'.format(coin_id)), 404

    coin = PortfolioCoin(result[0], int(result[1] if result[1] is not None else 0), _get_current_price(result[0]))
    return success(PortfolioCoinSchema().dump(coin))


@portfolio_coin_api.route('/<coin_id>/transactions')
def get_coin_transactions(portfolio_id, coin_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    if not _coin_exists(coin_id):
        return error('Coin with ID {} not found'.format(coin_id)), 404

    transactions = Transaction.query.filter_by(portfolio_id=portfolio_id, coin_id=coin_id).order_by(Transaction.timestamp)
    return success(TransactionSchema(many=True).dump(transactions))


@portfolio_coin_api.route('/<coin_id>/trades')
def get_coin_trades(portfolio_id, coin_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    if not _coin_exists(coin_id):
        return error('Coin with ID {} not found'.format(coin_id)), 404

    trades = Trade.query.filter(
        Trade.portfolio_id == portfolio_id,
        or_(
            Trade.from_coin_id == coin_id,
            Trade.to_coin_id == coin_id
        )
    ).order_by(Trade.timestamp)
    return success(TradeSchema(many=True).dump(trades))


def _get_current_price(coin):
    # TODO: if using is_fiat, then this line can be improved
    if coin.ref_exchange is None:
        return 1
    try:
        exchange = getattr(ccxt, coin.ref_exchange)()
        ticker = exchange.fetch_ticker('{}/EUR'.format(coin.symbol))
        return (ticker['ask'] + ticker['bid']) / 2
    except:
        return -1
