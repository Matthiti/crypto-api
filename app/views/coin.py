from flask import request, Blueprint
from marshmallow import ValidationError

from app import db
from app.models import Coin
from app.serializers import CoinSchema
from app.views import success, error

coin_api = Blueprint('Coin API', __name__, url_prefix='/api/v1/coins')


@coin_api.route('')
def all_coins():
    coins = Coin.query.order_by(Coin.symbol).all()
    return success(CoinSchema(many=True).dump(coins))


@coin_api.route('/<coin_id>')
def find_coin(coin_id):
    coin = _get_coin(coin_id)
    if coin is None:
        return error('Coin with ID {} not found'.format(coin_id)), 404
    return success(CoinSchema().dump(coin))


@coin_api.route('', methods=['POST'])
def create_coin():
    body = request.json
    schema = CoinSchema()

    try:
        coin = Coin(**schema.load(body))
    except ValidationError as err:
        return error('Invalid coin', details=err.messages), 400

    if _coin_with_symbol_exists(coin.symbol):
        return error('Coin with symbol {} already exists'.format(coin.symbol)), 409

    db.session.add(coin)
    db.session.commit()
    return success(schema.dump(coin)), 201


@coin_api.route('/<coin_id>', methods=['PUT'])
def update_coin(coin_id):
    coin = _get_coin(coin_id)
    if coin is None:
        return error('Coin with ID {} not found'.format(coin_id)), 404

    body = request.json
    schema = CoinSchema()

    try:
        props = schema.load(body)
    except ValidationError as err:
        return error('Invalid coin', details=err.messages), 400

    if props['symbol'] is not None and _other_coin_with_symbol_exists(coin.id, props['symbol']):
        return error('Coin with symbol {} already exists'.format(coin.symbol)), 409

    coin.update(props)
    db.session.commit()
    return success(schema.dump(coin))


@coin_api.route('/<coin_id>', methods=['DELETE'])
def delete_coin(coin_id):
    coin = _get_coin(coin_id)
    if coin is None:
        return error('Coin with ID {} not found'.format(coin_id)), 404

    db.session.delete(coin)
    db.session.commit()
    return '', 204


def _get_coin(coin_id):
    return Coin.query.get(coin_id)


def _coin_with_symbol_exists(symbol):
    return db.session.query(Coin.id).filter_by(symbol=symbol).scalar() is not None


def _other_coin_with_symbol_exists(coin_id, symbol):
    return db.session.query(Coin.id).filter(Coin.id != coin_id, Coin.symbol == symbol).scalar() is not None
