from flask import request, Blueprint
from marshmallow import ValidationError

from app import db
from app.models import Trade, Coin
from app.serializers import TradeSchema
from app.util import _portfolio_exists, _has_sufficient_amount
from app.views import success, error

portfolio_trade_api = Blueprint('Portfolio Trade API', __name__, url_prefix='/api/v1/portfolios/<portfolio_id>/trades')


@portfolio_trade_api.route('')
def all_trades(portfolio_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    trades = Trade.query.filter_by(portfolio_id=portfolio_id).order_by(Trade.timestamp)
    return success(TradeSchema(many=True).dump(trades))


@portfolio_trade_api.route('/<trade_id>')
def find_trade(portfolio_id, trade_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    trade = _get_trade(portfolio_id, trade_id)
    if trade is None:
        return error('Trade with ID {} not found'.format(trade_id)), 404

    return success(TradeSchema().dump(trade))


@portfolio_trade_api.route('', methods=['POST'])
def create_trade(portfolio_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    body = request.json
    schema = TradeSchema()

    try:
        trade = Trade(**schema.load(body))
    except ValidationError as err:
        return error('Invalid trade', details=err.messages), 400

    if not _coin_exists(trade.from_coin_id):
        return error('Unknown coin with ID {}'.format(trade.from_coin_id)), 400

    if not _coin_exists(trade.to_coin_id):
        return error('Unknown coin with ID {}'.format(trade.to_coin_id)), 400

    if not _has_sufficient_amount(portfolio_id, trade.from_coin_id, trade.from_amount):
        return error('Insufficient from amount'), 409

    trade.portfolio_id = portfolio_id
    db.session.add(trade)
    db.session.commit()
    return success(schema.dump(trade)), 201


@portfolio_trade_api.route('/<trade_id>', methods=['PUT'])
def update_trade(portfolio_id, trade_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    trade = _get_trade(portfolio_id, trade_id)
    if trade is None:
        return error('Trade with ID {} not found'.format(trade_id)), 404

    body = request.json
    schema = TradeSchema()

    try:
        props = schema.load(body)
    except ValidationError as err:
        return error('Invalid trade', details=err.messages), 400

    if props['from_coin_id'] is not None and not _coin_exists(props['from_coin_id']):
        return error('Unknown coin with ID {}'.format(props['from_coin_id'])), 400

    if props['to_coin_id'] is not None and not _coin_exists(props['to_coin_id']):
        return error('Unknown coin with ID {}'.format(props['to_coin_id'])), 400

    # TODO
    # if not _has_sufficient_amount(portfolio_id, trade.from_coin_id, trade.from_amount - old_from_amount):
    #     return error('Insufficient from amount'), 409

    trade.update(props)
    db.session.commit()
    return success(schema.dump(trade))


@portfolio_trade_api.route('/<trade_id>', methods=['DELETE'])
def delete_trade(portfolio_id, trade_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    trade = _get_trade(portfolio_id, trade_id)
    if trade is None:
        return error('Trade with ID {} not found'.format(trade_id)), 404

    db.session.delete(trade)
    db.session.commit()
    return '', 204


def _coin_exists(coin_id):
    return db.session.query(Coin.id).filter_by(id=coin_id).scalar() is not None


def _get_trade(portfolio_id, trade_id):
    return Trade.query.filter_by(id=trade_id, portfolio_id=portfolio_id).first()
