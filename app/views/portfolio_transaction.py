from flask import Blueprint, request
from marshmallow import ValidationError

from app import db
from app.models import Transaction
from app.serializers import TransactionSchema
from app.util import _portfolio_exists, _coin_exists
from app.views import success, error

portfolio_transaction_api = Blueprint('Portfolio Transaction API', __name__, url_prefix='/api/v1/portfolios/<portfolio_id>/transactions')


@portfolio_transaction_api.route('')
def all_transactions(portfolio_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    transactions = Transaction.query.filter_by(portfolio_id=portfolio_id).order_by(Transaction.timestamp)
    return success(TransactionSchema(many=True).dump(transactions))


@portfolio_transaction_api.route('/<transaction_id>')
def find_transaction(portfolio_id, transaction_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    transaction = _get_transaction(portfolio_id, transaction_id)
    if transaction is None:
        return error('Transaction with ID {} not found'.format(transaction_id)), 404

    return success(TransactionSchema().dump(transaction))


@portfolio_transaction_api.route('', methods=['POST'])
def create_transaction(portfolio_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    body = request.json
    schema = TransactionSchema()

    try:
        transaction = Transaction(**schema.load(body))
    except ValidationError as err:
        return error('Invalid transaction', details=err.messages), 400

    if not _coin_exists(transaction.coin_id):
        return error('Unknown coin with ID {}'.format(transaction.coin_id)), 400

    transaction.portfolio_id = portfolio_id
    db.session.add(transaction)
    db.session.commit()
    return success(schema.dump(transaction)), 201


@portfolio_transaction_api.route('/<transaction_id>', methods=['PUT'])
def update_transaction(portfolio_id, transaction_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(transaction_id)), 404

    transaction = _get_transaction(portfolio_id, transaction_id)
    if transaction is None:
        return error('Transaction with ID {} not found'.format(transaction_id)), 404

    body = request.json
    schema = TransactionSchema()

    try:
        props = schema.load(body)
    except ValidationError as err:
        return error('Invalid transaction', details=err.messages), 400

    if props['coin_id'] is not None and not _coin_exists(props['coin_id']):
        return error('Unknown coin with ID {}'.format(props['coin_id'])), 400

    transaction.update(props)
    db.session.commit()
    return success(schema.dump(transaction))


@portfolio_transaction_api.route('/<transaction_id>', methods=['DELETE'])
def delete_transaction(portfolio_id, transaction_id):
    if not _portfolio_exists(portfolio_id):
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    transaction = _get_transaction(portfolio_id, transaction_id)
    if transaction is None:
        return error('Transaction with ID not found'.format(transaction_id)), 404

    db.session.delete(transaction)
    db.session.commit()
    return '', 204


def _get_transaction(portfolio_id, transaction_id):
    return Transaction.query.filter_by(id=transaction_id, portfolio_id=portfolio_id).first()
