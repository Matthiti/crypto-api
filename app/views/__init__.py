def success(data):
    return {
        'success': True,
        'data': data
    }


def error(message, **kwargs):
    return {
        'success': False,
        'message': message,
        **kwargs
    }
