from flask import request, Blueprint, g
from marshmallow import ValidationError

from app import db
from app.models import Portfolio
from app.serializers import PortfolioSchema
from app.views import success, error

portfolio_api = Blueprint('Portfolio API', __name__, url_prefix='/api/v1/portfolios')


@portfolio_api.route('')
def all_portfolios():
    portfolios = Portfolio.query.filter_by(user_uuid=g.get('user_uuid'))
    return success(PortfolioSchema(many=True).dump(portfolios))


@portfolio_api.route('/<portfolio_id>')
def find_portfolio(portfolio_id):
    portfolio = _get_portfolio(portfolio_id)
    if portfolio is None:
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404
    return success(PortfolioSchema().dump(portfolio))


@portfolio_api.route('', methods=['POST'])
def create_portfolio():
    body = request.json
    schema = PortfolioSchema()

    try:
        portfolio = Portfolio(**schema.load(body))
    except ValidationError as err:
        return error('Invalid portfolio', details=err.messages), 400

    portfolio.user_uuid = g.get('user_uuid')
    db.session.add(portfolio)
    db.session.commit()
    return success(schema.dump(portfolio)), 201


@portfolio_api.route('/<portfolio_id>', methods=['PUT'])
def update_portfolio(portfolio_id):
    portfolio = _get_portfolio(portfolio_id)
    if portfolio is None:
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    body = request.json
    schema = PortfolioSchema()

    try:
        props = schema.load(body)
    except ValidationError as err:
        return error('Invalid portfolio', details=err.messages), 400

    portfolio.update(props)
    db.session.commit()
    return success(schema.dump(portfolio))


@portfolio_api.route('/<portfolio_id>', methods=['DELETE'])
def delete_portfolio(portfolio_id):
    portfolio = _get_portfolio(portfolio_id)
    if portfolio is None:
        return error('Portfolio with ID {} not found'.format(portfolio_id)), 404

    db.session.delete(portfolio)
    db.session.commit()
    return '', 204


def _get_portfolio(portfolio_id):
    return Portfolio.query.filter_by(
        user_uuid=g.get('user_uuid'),
        id=portfolio_id
    ).first()
