import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

from app.config import Config
from app.middleware import jwt_middleware

app = Flask(__name__)
app.config.from_object(Config)
app.before_request(jwt_middleware)
db = SQLAlchemy(app)
migrate = Migrate(app, db, directory=os.path.join('app', 'migrations'))

from app.views.coin import coin_api
from app.views.portfolio import portfolio_api
from app.views.portfolio_coin import portfolio_coin_api
from app.views.portfolio_trade import portfolio_trade_api
from app.views.portfolio_transaction import portfolio_transaction_api

app.register_blueprint(coin_api)
app.register_blueprint(portfolio_api)
app.register_blueprint(portfolio_coin_api)
app.register_blueprint(portfolio_trade_api)
app.register_blueprint(portfolio_transaction_api)
