from app import db


class BaseModel:

    def update(self, attributes: dict):
        for key, value in attributes.items():
            if hasattr(self, key):
                setattr(self, key, value)


class Coin(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)
    symbol = db.Column(db.String(8), nullable=False, unique=True)
    decimals = db.Column(db.Integer, nullable=False)
    ref_exchange = db.Column(db.String(64), nullable=True)
    buys = db.relationship('Trade', foreign_keys='Trade.to_coin_id', back_populates='to_coin')
    sells = db.relationship('Trade', foreign_keys='Trade.from_coin_id', back_populates='from_coin')
    transactions = db.relationship('Transaction', back_populates='coin')

    def __repr__(self):
        return '<Coin {}: {}>'.format(self.id, self.symbol)


class Trade(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    portfolio_id = db.Column(db.Integer, db.ForeignKey('portfolio.id'), nullable=False)
    portfolio = db.relationship('Portfolio', back_populates='trades')

    from_amount = db.Column(db.BigInteger, nullable=False)
    from_coin_id = db.Column(db.Integer, db.ForeignKey('coin.id'), nullable=False)
    from_coin = db.relationship('Coin', foreign_keys=[from_coin_id], back_populates='sells')

    to_amount = db.Column(db.BigInteger, nullable=False)
    to_coin_id = db.Column(db.Integer, db.ForeignKey('coin.id'), nullable=False)
    to_coin = db.relationship('Coin', foreign_keys=[to_coin_id], back_populates='buys')

    fee = db.Column(db.Integer, nullable=False, default=0)
    timestamp = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<Trade {}>'.format(self.id)


class Portfolio(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    user_uuid = db.Column(db.String(36), nullable=False)
    transactions = db.relationship('Transaction', back_populates='portfolio')
    trades = db.relationship('Trade', back_populates='portfolio')

    def __repr__(self):
        return '<Portfolio {}>'.format(self.id)


class PortfolioCoin:

    def __init__(self, coin, amount, current_price):
        self.coin = coin
        self.amount = amount
        self.current_price = current_price


class Transaction(db.Model, BaseModel):
    id = db.Column(db.Integer, primary_key=True)
    portfolio_id = db.Column(db.Integer, db.ForeignKey('portfolio.id'), nullable=False)
    portfolio = db.relationship('Portfolio', back_populates='transactions')
    amount = db.Column(db.BigInteger, nullable=False)
    coin_id = db.Column(db.Integer, db.ForeignKey('coin.id'))
    coin = db.relationship('Coin', back_populates='transactions')
    is_deposit = db.Column(db.Boolean, nullable=False)
    fee = db.Column(db.Integer, nullable=False, default=0)
    timestamp = db.Column(db.Integer, nullable=False)
