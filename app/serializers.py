from marshmallow import Schema, fields, validates_schema, ValidationError, pre_dump, post_load

from app.models import Coin, Portfolio, PortfolioCoin, Trade, Transaction


class ValidatedSchema:

    def validate_fields(self, required_attributes, data):
        missing_attributes = []
        for attribute in required_attributes:
            if attribute not in data or data[attribute] is None:
                missing_attributes.append(attribute)

        if len(missing_attributes) > 0:
            raise ValidationError("Missing required attribute(s): {}".format(
                ', '.join(["'{}'".format(attribute) for attribute in missing_attributes])))


class CoinSchema(Schema, ValidatedSchema):
    required_attributes = ['name', 'symbol', 'decimals']

    @validates_schema
    def validate_fields(self, data, **kwargs):
        super().validate_fields(self.required_attributes, data)

    class Meta:
        model = Coin
        fields = ('id', 'name', 'symbol', 'decimals', 'ref_exchange')
        dump_only = ('id',)


class PortfolioSchema(Schema, ValidatedSchema):
    required_attributes = ['name']

    @validates_schema
    def validate_fields(self, data, **kwargs):
        super().validate_fields(self.required_attributes, data)

    class Meta:
        model = Portfolio
        fields = ('id', 'name')
        dump_only = ('id',)


class PortfolioCoinSchema(Schema):
    coin = fields.Nested(CoinSchema())

    class Meta:
        model = PortfolioCoin
        fields = ('id', 'coin', 'amount', 'current_price')


class TradeSchema(Schema, ValidatedSchema):
    required_attributes = ['from_amount', 'from_coin_id', 'to_amount', 'to_coin_id', 'fee', 'timestamp']

    @validates_schema
    def validate_fields(self, data, **kwargs):
        super().validate_fields(self.required_attributes, data)

    class Meta:
        model = Trade
        fields = ('id', 'from_amount', 'from_coin_id', 'to_amount', 'to_coin_id', 'fee', 'timestamp')
        dump_only = ('id',)


class TransactionSchema(Schema, ValidatedSchema):
    required_attributes = ['amount', 'coin_id', 'is_deposit', 'fee', 'timestamp']

    @validates_schema
    def validate_fields(self, data, **kwargs):
        super().validate_fields(self.required_attributes, data)

    class Meta:
        model = Transaction
        fields = ('id', 'amount', 'coin_id', 'is_deposit', 'fee', 'timestamp')
        dump_only = ('id',)
