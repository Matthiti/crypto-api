"""Initalize coins

Revision ID: edc16b4f66d6
Revises: df89d8ca1dc0
Create Date: 2021-04-24 11:36:44.595153

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
from sqlalchemy import Table, MetaData

revision = 'edc16b4f66d6'
down_revision = 'df89d8ca1dc0'
branch_labels = None
depends_on = None

coin_table = Table('coin', MetaData(bind=op.get_bind()))


def upgrade():
    op.execute("INSERT INTO coin (name, symbol, decimals) VALUES ('Euro', 'EUR', 2)")


def downgrade():
    op.execute("DELETE FROM coin WHERE symbol = 'EUR'")
