import os


class Config:
    SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@%s:%s/%s' % (os.environ.get('DB_USERNAME'),
                                                          os.environ.get('DB_PASSWORD'),
                                                          os.environ.get('DB_HOST'),
                                                          os.environ.get('DB_PORT'),
                                                          os.environ.get('DB_NAME'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_PUBLIC_KEY = os.environ.get('JWT_PUBLIC_KEY').replace(r'\n', '\n')
    DEBUG = os.environ.get('DEBUG', False)
    DOMAIN_KEY = os.environ.get('DOMAIN_KEY')
